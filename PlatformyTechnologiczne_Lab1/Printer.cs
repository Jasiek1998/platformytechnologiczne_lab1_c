﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformyTechnologiczne_Lab1
{
    class Printer
    {
        public void PrintList(SortedList<string, long> sortedList)
        {
            foreach (var element in sortedList)
            {
                Console.WriteLine($"{element.Key} -> {element.Value}");
            }
        }

        public void PrintDirectory(string mainDirectory, int depth, bool recursively)
        {
            var stringBuilder = new StringBuilder();
            var directoryInfo = new DirectoryInfo(mainDirectory);
            string[] files = Directory.GetFiles(mainDirectory);
            string[] subDirectories = Directory.GetDirectories(mainDirectory);
            var fullSize = files.Length + subDirectories.Length;

            for (int i = 0; i < depth; i++)
            {
                stringBuilder.Append("\t");
            }
            stringBuilder.Append($"DIR: { directoryInfo.Name} SIZE: {fullSize} files RAHS: {directoryInfo.GetRahs()}");
            Console.WriteLine(stringBuilder);

            if (!recursively && depth > 0)
            {
                ;
            }
            else
            {
                foreach (string file in files)
                {
                    PrintFile(file, depth + 1);
                }
                foreach (string directory in subDirectories)
                {
                    PrintDirectory(directory, depth + 1, recursively);
                }
            }
        }

        public void PrintFile(string file, int depth)
        {
            var stringBuilder = new StringBuilder();
            var fileInfo = new FileInfo(file);

            for (int i = 0; i < depth; i++)
            {
                stringBuilder.Append("\t");
            }

            stringBuilder.Append($"FILE: { fileInfo.Name} SIZE: { fileInfo.Length} bytes RAHS: {fileInfo.GetRahs()}");
            Console.WriteLine(stringBuilder);
        }
    }
}
