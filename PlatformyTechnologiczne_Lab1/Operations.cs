﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace PlatformyTechnologiczne_Lab1
{
    class Operations
    {
        public SortedList<string, long> SortElements(string mainDirectory)
        {
            var sortedList = new SortedList<string, long>(new Comparator());
            string[] files = Directory.GetFiles(mainDirectory);
            string[] subDirectories = Directory.GetDirectories(mainDirectory);

            foreach (string file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                sortedList.Add(fileInfo.Name, fileInfo.Length);
            }

            foreach (string subDir in subDirectories)
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(subDir);
                sortedList.Add(directoryInfo.Name, Directory.GetFiles(subDir).ToArray().Length + Directory.GetDirectories(subDir).ToArray().Length);
            }

            return sortedList;
        }

        public void SerializationAndDeserialization(SortedList<string, long> sortedList, Printer printer)
        {
            using (var memoryStream = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter();

                binaryFormatter.Serialize(memoryStream, sortedList);
                memoryStream.Position = 0;

                var deserializedSoredList = binaryFormatter.Deserialize(memoryStream) as SortedList<string, long>;

                Console.WriteLine("\nAfter Deserialization:");
                printer.PrintList(deserializedSoredList);
            }
        }
    }
}
