﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformyTechnologiczne_Lab1
{
    public static class NewExtensions
    {
        public static DateTime GetOldestElement(this DirectoryInfo directoryInfo)
        {
            var date = directoryInfo.CreationTime;

            foreach (var dir in directoryInfo.GetDirectories())
            {
                var olderDate = dir.GetOldestElement();
                if (DateTime.Compare(date, olderDate) > 0)
                {
                    date = olderDate;
                }
            }

            var allFilesInfos = directoryInfo.EnumerateFiles().ToList();
            foreach (FileInfo fileInfo in allFilesInfos)
            {
                var fileDate = fileInfo.CreationTime;
                if (DateTime.Compare(date, fileDate) > 0)
                {
                    date = fileDate;
                }
            }

            return date;
        }

        public static string GetRahs(this FileSystemInfo fileSystemInfo)
        {
            FileAttributes attributes = File.GetAttributes(fileSystemInfo.FullName);
            var rahs = new StringBuilder();
            rahs.Append(((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly ? "R" : "-") +
                ((attributes & FileAttributes.Archive) == FileAttributes.Archive ? "A" : "-") +
                ((attributes & FileAttributes.Hidden) == FileAttributes.Hidden ? "H" : "-") +
                ((attributes & FileAttributes.System) == FileAttributes.System ? "S" : "-"));
            return rahs.ToString();
        }
    }
}
