﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace PlatformyTechnologiczne_Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            var directory = args[0];
            var recursively = false;
            Printer printer = new Printer();
            Operations operations = new Operations();
            if (args[1] == "-r")
            {
                recursively = true;
            }

            if (File.Exists(directory))
            {
                printer.PrintFile(directory, 0);
            }
            else if (Directory.Exists(directory))
            {
                printer.PrintDirectory(directory, 0, recursively);
            }
            else
            {
                Console.WriteLine("Not a file or directory given");
            }

            Console.WriteLine($"\nOldest element date: {new DirectoryInfo(directory).GetOldestElement().ToString()}\n");

            SortedList<string, long> sortedList = operations.SortElements(directory);
            Console.WriteLine("Before serialization:");
            printer.PrintList(sortedList);

            operations.SerializationAndDeserialization(sortedList, printer);
        }
    }
}
